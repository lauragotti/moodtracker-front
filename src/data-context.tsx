import {
  Dispatch,
  SetStateAction,
  createContext,
  useEffect,
  useState,
} from "react";
import { RatingDayI } from "./entities";
import { getLocalTimeZone, today } from "@internationalized/date";
import { fetchAllRatings } from "./service";
import router from "next/router";

interface DataState {
  allRatings: RatingDayI[] | undefined;
  selectedDate: string;
  setSelectedDate: Dispatch<SetStateAction<string>>;
}

export const DataContext = createContext({} as DataState);

export const DataContextProvider = ({ children }: any) => {
  const [allRatings, setAllRatings] = useState<RatingDayI[] | undefined>();
  let [selectedDate, setSelectedDate] = useState(
    today(getLocalTimeZone()).toString()
  );

  useEffect(() => {
    fetchAllRatings()
      .then((data) => {
        setAllRatings(data);
      })
      .catch((error) => {
        if (error.response.statuts == 401) {
          router.push("/login");
        }
      });
  }, []);

  return (
    <DataContext.Provider value={{ allRatings, selectedDate, setSelectedDate }}>
      {children}
    </DataContext.Provider>
  );
};
