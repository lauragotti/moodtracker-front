export interface BlocI {
  id?:number
}

export interface UserI {
  id?: number;
  name: string;
  email: string;
  password: string;
}

export interface RatingDayI {
  id?: number,
  rating: number,
  day: string,
  idUser?: number,
  comment: string
}