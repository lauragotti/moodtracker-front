import React, { useContext, useEffect, useState } from "react";
import Bloc from "@/components/Bloc";
import Header from "@/components/Header";
import { UserI } from "@/entities";
import { fetchUser } from "@/auth/auth-service";
import { useRouter } from "next/router";
import { DataContext } from "@/data-context";
import { AuthContext } from "@/auth/auth-context";

function IndexView() {
  const router = useRouter();
  const { token, setToken } = useContext(AuthContext);

  function logout() {
    setToken(null);
    router.push("/login");
  }

  const [user, setUser] = useState<UserI>({
    name: "",
    email: "",
    password: "",
  });

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.statuts == 401) {
          router.push("/login");
        }
      });
  }, []);

  const colClass: string =
    "col-md-6 d-flex flex-column justify-content-md-between";

  const { selectedDate } = useContext(DataContext);
  const formattedDate = new Date(selectedDate).toLocaleDateString("en-US", {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  });

  return (
    <>
      <div className="row">
        {/* First Column */}
        <div className={colClass}>
          <Header name={user.name} date={formattedDate} />
          <Bloc content="entry" />
          <Bloc content="calendar" />
        </div>

        {/* Second Column */}
        <div className={colClass}>
          <Bloc content="graph" />
          <Bloc content="stats" />
          <Bloc content="friends" />
        </div>
      </div>
      <div className="row align-content-end">
        <div className="text-end">
          <a onClick={logout} className="fs-2">
            logout
          </a>
        </div>
      </div>
    </>
  );
}

export default IndexView;
