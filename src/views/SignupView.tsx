import { AuthContext } from "@/auth/auth-context";
import { login, postUser } from "@/auth/auth-service";
import { UserI } from "@/entities";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useContext, useState } from "react";

function SignupView() {
  const [errors, setErrors] = useState("");
  const router = useRouter();
  const [user, setUser] = useState<UserI>({
    name: "",
    email: "",
    password: "",
  });
  const [passwordConfirmation, setPasswordConfirmation] = useState("");

  function handleChange(event: any) {
    setUser({
      ...user,
      [event.target.name]: event.target.value,
    });
  }
  async function handleSubmit(event: any) {
    event.preventDefault();
    setErrors("");
    if (user.password !== passwordConfirmation) {
      setErrors(
        "Le mot de passe et la confirmation du mot de passe ne correspondent pas."
      );
      return;
    }
    try {
      await postUser(user);
      router.push("/login");
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        if (error.response.data.errors) {
          const validationErrors = error.response.data.errors;
          const errorMessages = validationErrors.detail.split("\n");
          setErrors(errorMessages);
        } else {
          setErrors(error.response.data);
        }
      } else {
        setErrors("Server error");
      }
    }
  }

  return (
    <>
      <h1 className="h3 my-3">Create your account</h1>
      <div className="row justify-content-center">
        <div className="col-md-6">
          {errors && <p className="text-danger">{errors}</p>}
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Username*
              </label>
              <input
                type="text"
                name="name"
                value={user.name}
                onChange={handleChange}
                className="form-control"
                id="name"
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="email" className="form-label">
                Email*
              </label>
              <input
                type="email"
                name="email"
                value={user.email}
                onChange={handleChange}
                className="form-control"
                id="email"
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="password" className="form-label">
                Password* (8 characters minimum)
              </label>
              <input
                type="password"
                name="password"
                value={user.password}
                onChange={handleChange}
                className="form-control"
                id="password"
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="passwordConfirmation" className="form-label">
                Confirm password*
              </label>
              <input
                type="password"
                name="passwordConfirmation"
                value={passwordConfirmation}
                onChange={(event) =>
                  setPasswordConfirmation(event.target.value)
                }
                className="form-control"
                id="passwordConfirmation"
                required
              />
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-outline-dark w-50">
                Sign up
              </button>
            </div>
            <div className="text-center mt-3">
              <Link href="/login">Already have an account ? Log in</Link>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default SignupView;
