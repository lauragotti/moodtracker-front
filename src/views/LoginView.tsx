import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useContext, useState } from "react";

function LoginView() {
  const router = useRouter();
  const { token, setToken } = useContext(AuthContext);
  const [error, setError] = useState("");
  const [log, setLog] = useState({
    email: "",
    password: "",
  });

  function handleChange(event: any) {
    setLog({
      ...log,
      [event.target.name]: event.target.value,
    });
  }

  async function handleSubmit(event: any) {
    event.preventDefault();
    setError("");
    try {
      setToken(await login(log.email, log.password));
      router.push("/");
    } catch (error: any) {
      console.log(error);
      if (error.response?.status == 401) {
        setError("Email/password invalid");
      } else {
        setError("Server error");
      }
    }
  }
  return (
    <>
      <h1 className="h3 my-3">Log into your account</h1>
      <div className="row justify-content-center">
        {token ? (
          <button
            className="btn btn-outline-mood-two"
            onClick={() => setToken(null)}
          >
            Log out
          </button>
        ) : (
          <div className="col-md-6">
            {error && <p className="text-danger">{error}</p>}
            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <label htmlFor="email" className="form-label">
                  Email*
                </label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  aria-describedby="emailHelp"
                  name="email"
                  onChange={handleChange}
                  required
                />
              </div>
              <div className="mb-3">
                <label htmlFor="password" className="form-label">
                  Password*
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  name="password"
                  onChange={handleChange}
                  required
                />
              </div>

              <div className="text-center">
                <button type="submit" className="btn btn-outline-dark w-50">
                  Submit
                </button>
              </div>
            </form>
            <div className="text-center">
              <button className="btn btn-outline-mood-two mt-3 w-50">
                <Link href="/signup" className="text-decoration-none">
                  Sign up
                </Link>
              </button>
            </div>
          </div>
        )}
      </div>
    </>
  );
}

export default LoginView;
