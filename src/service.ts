import { RatingDayI } from "@/entities";
import axios from "axios";

export async function fetchAllRatings() {
    const response = await axios.get<RatingDayI[]>('/api/ratingday');
    return response.data;
}

export async function fetchRatingDay(day:string) {
  const response = await axios.get<RatingDayI>('/api/ratingday/day/'+day);
  return response.data;
}
