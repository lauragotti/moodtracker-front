import "@/styles/globals.css";
import type { AppProps } from "next/app";
import "../styles/custom.scss";
import "../auth/axios-config";
import "../../node_modules/bootstrap-icons/font/bootstrap-icons.css";
import Head from "next/head";
import { useEffect } from "react";
import { IBM_Plex_Mono } from "next/font/google";
import { AuthContextProvider } from "@/auth/auth-context";
import { DataContextProvider } from "@/data-context";
const ibm = IBM_Plex_Mono({
  subsets: ["latin"],
  weight: ["200", "300", "400", "500"],
});
export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);
  return (
    <>
      <Head>
        <title>Moodtracker</title>
        <meta name="description" content="Moodtracker" />
      </Head>
      <AuthContextProvider>
        <DataContextProvider>
          <main className={`${ibm.className} container-fluid text-black`}>
            <div className="row justify-content-center p-1 p-md-5 main-row">
              <Component {...pageProps} />
            </div>
          </main>
        </DataContextProvider>
      </AuthContextProvider>
    </>
  );
}
