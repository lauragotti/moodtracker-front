import IndexView from "@/views/IndexView";
import React from "react";

function index() {
  return <IndexView />;
}

export default index;
