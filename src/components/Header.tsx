import { UserI } from "@/entities";
import { Press_Start_2P } from "next/font/google";
import React from "react";
const pressstart = Press_Start_2P({
  subsets: ["latin"],
  weight: ["400"],
});

interface Props {
  name: string;
  date: string;
}
function Header({ name, date }: Props) {
  return (
    <div className="p-1">
      <p className={`${pressstart.className}`}>{date}</p>
      <p>Hi {name}, how is your day ?</p>
    </div>
  );
}

export default Header;
