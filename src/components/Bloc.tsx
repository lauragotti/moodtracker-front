import { BlocI } from "@/entities";
import React, { useState } from "react";
import Smileys from "./Smileys";
import EntryContent from "./EntryContent";
import CalendarContent from "./CalendarContent";
import GraphContent from "./GraphContent";
import StatsContent from "./StatsContent";
import FriendsContent from "./FriendsContent";

interface Props {
  content: "entry" | "calendar" | "graph" | "stats" | "friends";
}

function Bloc({ content }: Props) {
  const bloctype = () => {
    switch (content) {
      case "entry":
        return <EntryContent />;
      case "calendar":
        return <CalendarContent />;
      case "graph":
        return <GraphContent />;
      case "stats":
        return <StatsContent />;
      case "friends":
        return <FriendsContent />;
      default:
        return null;
    }
  };
  return (
    <div className="pb-3">
      <div className="blocs py-3 px-4">{bloctype()}</div>
    </div>
  );
}

export default Bloc;
