import React, { useContext } from "react";
import {
  Button,
  Calendar,
  CalendarCell,
  CalendarGrid,
  Heading,
} from "react-aria-components";
import { parseDate } from "@internationalized/date";
import { DataContext } from "@/data-context";
import { colorMatch } from "./utils/color.utils";

function CalendarContent() {
  const { selectedDate, setSelectedDate, allRatings } = useContext(DataContext);
  const handleDateChange = (newDate: any) => {
    setSelectedDate(newDate.toString());
  };

  //to move
  //check si ya une entry
  function isEntry(date: string) {
    return !!allRatings?.find((ratingDay) => ratingDay.day === date); //utiliser fetch ratind day mtn
  }

  return (
    <Calendar
      aria-label="Selected date"
      value={parseDate(selectedDate)}
      onChange={handleDateChange}
      defaultFocusedValue={parseDate(selectedDate)}
      className="d-flex flex-column "
    >
      <header className="d-flex justify-content-between">
        <Button slot="previous">◀</Button>
        <Heading />
        <Button slot="next">▶</Button>
      </header>
      <CalendarGrid className="text-center text">
        {(date) => {
          const ratingDay = allRatings?.find(
            (ratingDay) => ratingDay.day === date.toString()
          );
          const moodClass = ratingDay ? colorMatch(ratingDay.rating) : "";

          return (
            <CalendarCell
              date={date}
              className={({ isSelected, isDisabled }) =>
                isSelected
                  ? "bg-primary"
                  : isDisabled
                  ? "disabled text-black-50"
                  : isEntry(date.toString())
                  ? "bg-" + moodClass
                  : ""
              }
            />
          );
        }}
      </CalendarGrid>
    </Calendar>
  );
}

export default CalendarContent;
