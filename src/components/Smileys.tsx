import { DataContext } from "@/data-context";
import { RatingDayI } from "@/entities";
import { fetchRatingDay } from "@/service";
import router from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { colorMatch } from "./utils/color.utils";

function Smileys() {
  const [emojis, setEmojis] = useState<{ key: number; className: string }[]>([
    { key: 1, className: "bi-emoji-dizzy" },
    { key: 2, className: "bi-emoji-frown" },
    { key: 3, className: "bi-emoji-neutral" },
    { key: 4, className: "bi-emoji-smile" },
    { key: 5, className: "bi-emoji-grin" },
  ]);

  const { selectedDate } = useContext(DataContext);
  const [ratingDay, setRatingDay] = useState<RatingDayI>();

  useEffect(() => {
    fetchRatingDay(selectedDate)
      .then((data) => {
        setRatingDay(data);
      })
      .catch((error) => {
        if (error.response.status === 404) {
          setRatingDay(undefined);
        }
        if (error.response.status === 401) {
          router.push("/login");
        }
      });
  }, [selectedDate]);

  function getColorClass() {
    if (ratingDay && ratingDay.rating) {
      return "text-" + colorMatch(ratingDay.rating);
    }
    return "";
  }

  return (
    <div className="row text-center">
      {emojis.map((emoji) => (
        <div key={emoji.key} className="col d-flex justify-content-center">
          <i
            className={`bi ${
              ratingDay?.rating === emoji.key
                ? emoji.className + "-fill" + " " + getColorClass()
                : emoji.className
            } fs-8`}
          />
        </div>
      ))}
    </div>
  );
}

export default Smileys;
