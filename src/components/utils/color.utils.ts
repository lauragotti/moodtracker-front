export function colorMatch(rating: number) {
  if (rating === 1) {
    return "mood-one";
  } else if (rating === 2) {
    return "mood-two";
  } else if (rating === 3) {
    return "mood-three";
  } else if (rating === 4) {
    return "mood-four";
  } else if (rating === 5) {
    return "mood-five";
  } else {
    return false;
  }
}
